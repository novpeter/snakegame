﻿using System;
using System.IO;

namespace SnakeGame
{
    class Menu
    {
        public static bool startGame = false;
        public static bool check = true;

        public static void Draw()
        {
            DrawGameLogo();
            Start();
            while (!startGame)
                TouchReaction();
        }

        private static void DrawGameLogo()
        {
            string[] gameFieldInStr = File.ReadAllLines(@"menu.txt");
            int x = gameFieldInStr[1].Length;
            int y = gameFieldInStr.Length;
            
            Char[,] GameField = new char[x, y]; // создаем двумерную матрицу представляющую из себя игровое поле 
            for (int i = 0; i < y; i++)
            {
                var currientString = gameFieldInStr[i].ToCharArray();
                for (int j = 0; j < x; j++)
                    GameField[j, i] = currientString[j];
            }

            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                    Console.Write(GameField[j, i]);
                Console.WriteLine();
            }
        }

        public static void Start()
        {
            Coursor();
            CreatMenu();
        }

        public static void Coursor()
        {
            Console.SetCursorPosition(30, 12);
            Console.Write(">");
        }

        public static void CoursorGoUp()
        {
            Console.SetCursorPosition(30, 15);
            Console.Write(" ");
            Console.SetCursorPosition(30, 12);
            Console.Write(">");
        }

        public static void CoursorGoDown()
        {
            Console.SetCursorPosition(30, 12);
            Console.Write(" ");
            Console.SetCursorPosition(30, 15);
            Console.Write(">");
        }

        public static void CreatMenu(string name1 = "Start Game", string name2 = "Statistics")
        {
            Console.SetCursorPosition(32, 12);
            Console.Write(name1);
            Console.SetCursorPosition(32, 15);
            Console.Write(name2);
        }

        public static void TouchReaction()
        {
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.UpArrow:
                    check = true;
                    CoursorGoUp();
                    break;
                case ConsoleKey.DownArrow:
                    check = false;
                    CoursorGoDown();
                    break;
                case ConsoleKey.Enter:
                    if (check)
                    {
                        Console.Clear();
                        startGame = true;
                    }
                    else
                    {
                        Console.Clear();
                        using (StreamReader sr = new StreamReader("input.txt"))
                        {
                            string line = sr.ReadToEnd();
                            Console.WriteLine(line);
                            while (!startGame)
                                switch (Console.ReadKey(true).Key)
                                {
                                    case ConsoleKey.Escape:
                                        Console.Clear();
                                        check = true;
                                        Draw();
                                        break;
                                }
                        }
                    }
                    break;
            }
        }
    }
}
