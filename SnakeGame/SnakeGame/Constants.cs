﻿using System;
namespace SnakeGame
{
    public class Constants
    {
        //Screen
        public static int WindowWidth { get { return 80; } }
        public static int WindowHeigth { get { return 20; } }
        public static int MapWidth { get { return 56; } }
        public static int MapHeigth { get { return 20; } }
        public static int ScoreMenuHeigth { get { return 30; } }
        public static int ScoreMenuWidth { get { return 24; } }

        //Border
        public static char BorderSymbol { get { return '#'; } }
        public static ConsoleColor BorderColor { get { return ConsoleColor.Gray; } }

        //Snake
        public static char SnakeSymbol { get { return '*'; } }
        public static ConsoleColor SnakeColor { get { return ConsoleColor.Red; } }
        public static int SnakeStartLength { get { return 4; } }
        public static int SnakeThreadSleep { get; set; } 

        //Food
        public static char FoodSymbol { get { return '$'; } }
        public static ConsoleColor FoodColor { get { return ConsoleColor.Green; } }

        //Fonts
        public static ConsoleColor MenuFontColor { get { return ConsoleColor.DarkRed; } }
        public static char SelectSymbol { get { return '>'; } }
        public static ConsoleColor SelectSymbolColor { get { return ConsoleColor.Yellow; } }

        //Strings
        public static string Score { get { return "SCORE: "; } }
        public static string GameStatus { get { return "STATUS: "; } }
        public static string GameStatusPause { get { return "PAUSED  "; } }
        public static string GameStatusPlay { get { return "PLAYING"; } }
        public static string GameTime { get { return "TIME: "; } }
        public static string GameStatusLose { get { return "LOSE!  "; } }
        public static string PauseHint { get { return "Pause - press Esc"; } }
        public static string ExitHint { get { return "Exit - press Q"; } }

        //Strings colors
        public static ConsoleColor GameStatusPauseColor { get { return ConsoleColor.Red; } }
        public static ConsoleColor GameStatusPlayColor { get { return ConsoleColor.Green; } }
        public static ConsoleColor LoseGameColor { get { return ConsoleColor.Yellow; } }
    }
}
