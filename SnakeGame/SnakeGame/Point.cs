﻿using System;

namespace SnakeGame
{
    public class Point
    {
        public int x;
        public int y;
        public char symbol;
        
        public Point() {}

        public Point(int x, int y, char symbol)
        {
            this.x = x;
            this.y = y;
            this.symbol = symbol;
        }

        public void Move(int delta, Direction direction)
        {
            if (direction == Direction.Right)
            {
                x = x + delta;
            }
            else if (direction == Direction.Left)
            {
                x = x - delta;
            }
            else if (direction == Direction.Up)
            {
                y = y - delta;
            }
            else if (direction == Direction.Down)
            {
                y = y + delta;
            }
        }

        public Point(Point p)
        {
            x = p.x;
            y = p.y;
            symbol = p.symbol;
        }

        public void Clear()
        {
            Console.SetCursorPosition(x, y);
            Console.Write(' ');
        }

        public void Draw()
        {
            Console.SetCursorPosition(x, y);
            Console.Write(symbol); 
        }

        public bool IsConflict(Point p) => this.x == p.x && this.y == p.y;
    }
}
