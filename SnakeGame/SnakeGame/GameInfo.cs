﻿using System;
using System.Diagnostics;

namespace SnakeGame
{
    public class GameInfo
    {
        public void DrawInfo(Snake snake, int score, Stopwatch time)
        {
            DrawScore(score);
            DrawTime(time);
            DrawGameStatus(snake);
            DrawHints();
        }

        private void DrawScore(int score)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(58, 4);
            Console.Write($"{Constants.Score}{score}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(65, 4);
            Console.Write($"{score}");
        }

        private void DrawTime(Stopwatch time)
        {
            TimeSpan ts = time.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}.{2:00}",
                                               ts.Minutes, ts.Seconds,
                                               ts.Milliseconds / 10);
            Console.SetCursorPosition(58, 6);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"{Constants.GameTime}{elapsedTime}");
        }

        private void DrawGameStatus(Snake snake)
        {
            Console.SetCursorPosition(58, 8);
            Console.Write(Constants.GameStatus);
            string status = "";
            switch (snake.status)
            {
                case SnakeGame.GameStatus.GameOver:
                    Console.ForegroundColor = ConsoleColor.Red;
                    status = Constants.GameStatusLose;
                    break;
                case SnakeGame.GameStatus.Pause:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    status = Constants.GameStatusPause;
                    break;
                case SnakeGame.GameStatus.Playing:
                    Console.ForegroundColor = ConsoleColor.Green;
                    status = Constants.GameStatusPlay;
                    break;
                case SnakeGame.GameStatus.Exit:
                    return;
            }
            Console.SetCursorPosition(65, 8);
            Console.WriteLine(status);
        }

        private void DrawHints()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(58, 14);
            Console.WriteLine(Constants.PauseHint);
            Console.SetCursorPosition(58, 16);
            Console.WriteLine(Constants.ExitHint);
            Console.ForegroundColor = Constants.SnakeColor;
        }
    }
}
