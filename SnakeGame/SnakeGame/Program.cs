﻿using System;
using System.Threading;
using System.Diagnostics;

namespace SnakeGame
{
    class MainClass
    {
        public static void Main()
        {
            Console.SetWindowSize(Constants.WindowWidth, Constants.WindowHeigth);
            Console.CursorVisible = false;
            Menu.Draw();
            StartGame();
            Console.ReadKey();
        }

        private static void StartGame()
        {
            int score = 0;
            Stopwatch time = new Stopwatch();
            GameInfo info = new GameInfo();
            time.Start();
            Constants.SnakeThreadSleep = 120;
            Console.CursorVisible = false;
            Console.ForegroundColor = Constants.BorderColor;
            Walls walls = new Walls(Constants.MapWidth ,Constants.MapHeigth);

            Console.ForegroundColor = Constants.SnakeColor;
            Point head = new Point(10, 3, Constants.SnakeSymbol);
            Snake snake = new Snake(head, 5, Direction.Right);
            snake.Draw();

            Console.ForegroundColor = Constants.FoodColor;
            FoodGenerator foodGenerator = new FoodGenerator(Constants.MapWidth, Constants.MapHeigth);
            Point food = foodGenerator.CreateFood();
            food.Draw();
            Console.ForegroundColor = Constants.SnakeColor;

            while (true)
            {
                info.DrawInfo(snake, score, time);
                if (snake.status == GameStatus.Exit)
                {
                    Console.Clear();
                    return;   
                }
                
                if (snake.IsHitTail() || walls.IsConflict(snake))
                {
                    info.DrawInfo(snake, score, time);
                    snake.status = GameStatus.GameOver;
                    break;
                }
                if (snake.Eat(food))
                {
                    Console.ForegroundColor = Constants.FoodColor;
                    food = foodGenerator.CreateFood();
                    food.Draw();
                    score++;
                    info.DrawInfo(snake, score, time);
                    if (Constants.SnakeThreadSleep > 10) Constants.SnakeThreadSleep-=5;
                }
                snake.Move();
                Thread.Sleep(Constants.SnakeThreadSleep);
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    snake.ReactionToKey(key.Key,ref time);
                    Console.ForegroundColor = Constants.FoodColor;
                    food.Draw();
                    Console.ForegroundColor = Constants.SnakeColor;
                }
            }
            Console.Clear();
        }
    }
}
