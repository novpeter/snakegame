﻿using System;

namespace SnakeGame
{
    public enum Direction
    {
        Left,
        Right,
        Down,
        Up
    }
}
