﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SnakeGame
{
    public enum Direction
    {
        Left,
        Right,
        Down,
        Up
    }

    public enum GameStatus
    {
        GameOver,
        Pause,
        Playing,
        Exit
    }

   public class Snake : Figure
    {
        Direction direction;
        public GameStatus status;

        bool freeze = false;

        public Snake(Point tail, int length, Direction _direction)
        {
            status = GameStatus.Playing;
            direction = _direction;
            pointsList = new List<Point>();
            for (int i = 0; i < length; i++)
            {
                Point p = new Point(tail);
                p.Move(i, direction);
                pointsList.Add(p);
            }
        }

        public void Move()
        {
            if (!freeze)
            {
                Point tail = pointsList.First();
                pointsList.Remove(tail);
                Point head = GetNextPoint();
                pointsList.Add(head);

                tail.Clear();
                head.Draw();
                Thread.Sleep(Constants.SnakeThreadSleep);
            }
        }

        public Point GetNextPoint()
        {
            Point head = pointsList.Last();
            Point nextPoint = new Point(head);
            nextPoint.Move(1, direction);
            return nextPoint;
        }

        internal bool Eat(Point food)
        {
            Point head = GetNextPoint();
            if (head.IsConflict(food))
            {
                food.symbol = head.symbol;
                food.Draw();
                pointsList.Add(food);
                return true;
            }
            else return false;
        }

        public bool IsHitTail()
        {
            var head = pointsList.Last();
            for (int i = 0; i < pointsList.Count - 2; i++)
            {
                if (head.IsConflict(pointsList[i]))
                    return true;
            }
            return false;
        }


        public void ReactionToKey(ConsoleKey key, ref Stopwatch time)
        {
            if (key == ConsoleKey.LeftArrow && direction != Direction.Right)
                direction = Direction.Left;
            else if (key == ConsoleKey.RightArrow && direction != Direction.Left)
                direction = Direction.Right;
            else if (key == ConsoleKey.DownArrow && direction != Direction.Up)
                direction = Direction.Down;
            else if (key == ConsoleKey.UpArrow && direction != Direction.Down)
                direction = Direction.Up;
            else if (key == ConsoleKey.Escape)
            {
                if (freeze != false)
                {
                    status = GameStatus.Playing;
                    time.Start();
                }
                else
                {
                    status = GameStatus.Pause;
                    time.Stop();
                }
                freeze = !freeze;
            }
            else if (key == ConsoleKey.Q)
            {
                status = GameStatus.Exit;
            }
        }
    }
}
