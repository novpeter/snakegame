﻿using System;
using System.Collections.Generic;

namespace SnakeGame
{
    public class Walls
    {
        List<Figure> wallList;

        public Walls( int mapWidth, int mapHeight )
        {
            wallList = new List<Figure>();
            var scoreBorderList = new List<Figure>();

            HorizontalLine upLine = new HorizontalLine( 0, Constants.WindowWidth - 2, 0, Constants.BorderSymbol );
            HorizontalLine downLine = new HorizontalLine( 0, Constants.WindowWidth - 2, mapHeight - 1, Constants.BorderSymbol );
            VerticalLine leftLine = new VerticalLine( 0, mapHeight - 1, 0, Constants.BorderSymbol );
            VerticalLine rightLine = new VerticalLine( 0, mapHeight - 1, mapWidth - 2, Constants.BorderSymbol);
            VerticalLine rightScoreBorder = new VerticalLine(0, mapHeight - 1, Constants.WindowWidth - 2, Constants.BorderSymbol );

            wallList.Add( upLine );
            wallList.Add( downLine );
            wallList.Add( leftLine );
            wallList.Add( rightLine );
            foreach( var line in wallList) line.Draw();
            rightScoreBorder.Draw();
        }

        internal bool IsConflict(Figure figure)
        {
            foreach (var wall in wallList)
            {
                if (wall.IsConflict(figure))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
