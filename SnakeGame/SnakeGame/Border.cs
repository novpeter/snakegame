﻿using System;
using System.Collections.Generic;
namespace SnakeGame
{
    public class Border
    {
        private int width;
        private int heigth;
        private char symbol;

        public Border(int width, int heigth, char symbol)
        {
            this.width = width;
            this.heigth = heigth;
            this.symbol = symbol;
        }

        public void DrawBorders()
        {
            CreateHorizontalLine(0, width-1, 0, symbol);
            CreateHorizontalLine(0, width-1, heigth-1, symbol);
            CreateVerticalLine(0, heigth-1, 0, symbol);
            CreateVerticalLine(0, heigth-1, width-1, symbol);
        }

        private void CreateVerticalLine(int yUp, int yDown, int x, char symbol)
        {
            List<Point> line = new List<Point>();
            for (int y = yUp; y<=yDown; y++)
            {
                Point point = new Point(x, y, symbol);
                line.Add(point);
            }
            Draw(line);
        }

        private void CreateHorizontalLine(int xLeft, int xRight, int y, char symbol)
        {
            List<Point> line = new List<Point>();
            for (int x = xLeft; x<=xRight; x++)
            {
                Point point = new Point(x, y, symbol);
                line.Add(point);
            }
            Draw(line);
        }
        
        private void Draw(List<Point> points)
        {
            foreach(var point in points)
                point.Draw();
        }
    }
}
