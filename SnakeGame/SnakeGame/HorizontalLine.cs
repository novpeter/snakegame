﻿using System.Collections.Generic;
using SnakeGame;

namespace SnakeGame
{
    class HorizontalLine : Figure
    {
        public HorizontalLine(int xLeft, int xRight, int y, char symbol)
        {
            pointsList = new List<Point>();
            for(int x = xLeft; x <= xRight; x++)
            {
                Point p = new Point( x, y, symbol );
                pointsList.Add( p );
            }           
        }
    }
}