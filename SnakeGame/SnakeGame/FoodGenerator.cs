﻿using System;
namespace SnakeGame
{
    public class FoodGenerator
    {
        int width;
        int heigth;
        Random random;

        public FoodGenerator(int mapWidth, int mapHeigth)
        {
            width = mapWidth;
            heigth = mapHeigth;
            random = new Random();
        }

        public Point CreateFood()
        {
            int x = random.Next(2, width - 2);
            int y = random.Next(2, heigth - 2);
            return new Point(x, y, Constants.FoodSymbol);
        }
    }
}
