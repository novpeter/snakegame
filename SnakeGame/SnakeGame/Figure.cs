﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGame
{
	public class Figure
	{
		protected List<Point> pointsList;

		public void Draw()
		{
			foreach (Point p in pointsList )
				p.Draw();
		}

        internal bool IsConflict(Figure figure)
        {
            foreach (var p in pointsList)
            {
                if (figure.IsConflict(p))
                    return true;
            }
            return false;
        }

        public bool IsConflict(Point point)
        {
            foreach (var p in pointsList)
            {
                if (p.IsConflict(point))
                    return true;
            }
            return false;
        }
    }
}